/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/25 13:52:37 by cumberto          #+#    #+#             */
/*   Updated: 2016/12/06 21:01:25 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *big, const char *little)
{
	size_t	i;
	size_t	j;
	char	*b;
	char	*l;

	i = 0;
	j = 0;
	l = (char *)little;
	b = (char *)big;
	if (l[0] == '\0')
		return (b);
	while (b[i] != '\0')
	{
		j = 0;
		if (b[i] == l[j])
		{
			while (b[i + j] == l[j] && b[i + j])
			{
				if (l[++j] == '\0')
					return (&b[i]);
			}
		}
		i++;
	}
	return (NULL);
}
