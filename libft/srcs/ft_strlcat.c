/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/24 15:35:27 by cumberto          #+#    #+#             */
/*   Updated: 2016/12/05 18:43:06 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	k;
	size_t	i;
	size_t	tmp;

	i = ft_strlen(dst);
	tmp = i;
	k = 0;
	while (i < size - 1 && src[k])
	{
		dst[i] = src[k];
		i++;
		k++;
	}
	if (i <= size)
		dst[i] = '\0';
	if (tmp > size)
		return (ft_strlen(src) + size);
	return (ft_strlen(src) + tmp);
}
