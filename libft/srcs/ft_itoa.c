/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/01 18:46:58 by cumberto          #+#    #+#             */
/*   Updated: 2016/12/06 18:51:27 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	int_len(unsigned int n)
{
	int				len;

	len = 0;
	if (n == 0)
		return (1);
	while (n > 0)
	{
		n = n / 10;
		len++;
	}
	return (len);
}

static int	if_negative(int num)
{
	if (num < 0)
		return (1);
	return (0);
}

char		*ft_itoa(int n)
{
	char			*str;
	int				neg;
	int				size;
	unsigned int	num;

	num = n < 0 ? -n : n;
	neg = 0;
	size = int_len(num);
	neg = if_negative(n);
	if ((str = ft_strnew((size_t)(size + neg))) == NULL)
		return (NULL);
	if (neg == 1)
		str[0] = '-';
	if (num == 2147483648)
		return (ft_strdup("-2147483648"));
	if (num == 0)
		str[neg] = '0';
	while (num > 0)
	{
		str[size - 1 + neg] = 48 + (num % 10);
		num = num / 10;
		size--;
	}
	return (str);
}
