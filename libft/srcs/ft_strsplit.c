/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/01 17:01:06 by cumberto          #+#    #+#             */
/*   Updated: 2016/12/05 19:09:00 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_words_counter(const char *s, char c)
{
	int		cnt;
	int		tmp;

	tmp = 0;
	cnt = 0;
	while (*s != '\0')
	{
		if (tmp == 1 && *s == c)
			tmp = 0;
		if (tmp == 0 && *s != c)
		{
			tmp = 1;
			cnt++;
		}
		s++;
	}
	return (cnt);
}

static int	ft_wordlen(const char *s, char c)
{
	int		len;

	len = 0;
	while (*s != c && *s != '\0')
	{
		len++;
		s++;
	}
	return (len);
}

char		**ft_strsplit(char const *s, char c)
{
	char	**str;
	int		nb_word;
	int		index;

	if (s == NULL)
		return (NULL);
	index = 0;
	nb_word = ft_words_counter((const char *)s, c);
	str = (char **)malloc(sizeof(*str) *
			(ft_words_counter((const char *)s, c) + 1));
	if (str == NULL)
		return (NULL);
	while (nb_word--)
	{
		while (*s == c && *s != '\0')
			s++;
		str[index] = ft_strsub((const char *)s, 0,
				ft_wordlen((const char *)s, c));
		if (str[index] == NULL)
			return (NULL);
		s = s + ft_wordlen(s, c);
		index++;
	}
	str[index] = NULL;
	return (str);
}
