/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putwchar_str.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/22 15:22:17 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/08 12:39:41 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putwchar_str(int fd, wchar_t *str)
{
	int cnt;

	cnt = 0;
	while (str[cnt])
	{
		ft_putwchar(fd, str[cnt]);
		cnt++;
	}
}
