/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_width.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/20 18:53:38 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/08 12:41:18 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	print_char(char chr, int num)
{
	int cnt;

	cnt = 0;
	while (cnt < num)
	{
		write(g_fd, &chr, 1);
		cnt++;
	}
}

void	print_str(t_print *elem, char *str, int size)
{
	if (elem->flag_m == 1)
	{
		write(g_fd, str, size);
		print_char(elem->chr, elem->width - size);
	}
	else
	{
		print_char(elem->chr, elem->width - size);
		write(g_fd, str, size);
	}
}
