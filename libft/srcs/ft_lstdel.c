/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/06 16:42:05 by cumberto          #+#    #+#             */
/*   Updated: 2016/12/06 18:51:58 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	if (alst == NULL || del == NULL)
		return ;
	while (*alst != NULL)
	{
		del((**alst).content, (**alst).content_size);
		free(*alst);
		*alst = (**alst).next;
	}
	free(*alst);
	*alst = NULL;
}
