#include <unistd.h>
#include <stdio.h>

int main()
{
  char *ls_arg[] = {"ls", "-lR", "/", NULL};
  char *grep_arg[] = {"grep", "Feb", NULL};
  // int pipefd[2];
  int pid;

  printf("A");
  // pipe(pipefd);
  pid = fork();
  if (pid == 0)
  {
    printf("B");
  }
  else
  {
    printf("C");
  }
  return 0;
}

// int main()
// {
//   char *ls_arg[] = {"ls", "-lR", "/", NULL};
//   char *grep_arg[] = {"grep", "Feb", NULL};
//   int pipefd[2];
//   int pid;
//
//   printf("A");
//   pipe(pipefd);
//   pid = fork();
//   if (pid == 0)
//   {
//     printf("B");
//
//       // close(pipefd[0]);    // close reading end in the child
//       //
//       // dup2(pipefd[1], 1);  // send stdout to the pipe
//       //
//       // close(pipefd[1]);    // this descriptor is no longer needed
//       //
//       // execve("/bin/ls", ls_arg, NULL);
//   }
//   else /*if (fork() == 0)*/
//   {
//     printf("C");
//
//     // close(pipefd[1]);
//     // dup2(pipefd[0], 0);
//     // close(pipefd[0]);
//     // execve("/bin/grep", grep_arg, NULL);
//   }
// /*  else
//   {
//
//       char buffer[4];
//
//       close(pipefd[1]);  // close the write end of the pipe in the parent
//
//       while (read(pipefd[0], buffer, sizeof(buffer)) != 0)
//       {
//         printf("%s", buffer);
//       }
//   }
// */
//
//
//   return 0;
// }
