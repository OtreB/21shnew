/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   history.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <cumberto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:31:22 by cumberto          #+#    #+#             */
/*   Updated: 2018/04/30 02:35:56 by tchapka          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void			ft_arrow_up(t_edit *line)
{
	int			cnt;
	int			up;

	cnt = 0;
	if (!line->h_first || line->cursor < line->max_size \
			|| (line->cursor > 0 && line->h_curr == NULL))
		return ;
	if (!line->h_curr)
		line->h_curr = line->h_last;
	else if (line->h_curr->up)
		line->h_curr = line->h_curr->up;
	ft_printf("\033[%dG", line->prompt_len + 1);
	if ((up = (line->prompt_len + line->max_size) / line->col) > 0)
		CURSOR_UP(up);
	ft_printf("\033[J");
	ft_printf("%s", line->h_curr->cmd);
	line->cmd = ft_strcat(reset_cmd(line->cmd, SIZE), line->h_curr->cmd);
	line->max_size = ft_strlen(line->h_curr->cmd);
	line->cursor = line->max_size;
}

void			ft_arrow_down(t_edit *line)
{
	int			cnt;
	int			up;

	cnt = 0;
	if (line->cursor == line->max_size && line->h_curr)
	{
		ft_printf("\033[%dG", line->prompt_len + 1);
		if ((up = (line->prompt_len + line->max_size) / line->col) > 0)
			ft_printf("\033[%dA", up);
		ft_printf("\033[J");
		line->h_curr = line->h_curr->down;
		if (line->h_curr)
		{
			ft_printf("%s", line->h_curr->cmd);
			line->cmd = ft_strcat(reset_cmd(line->cmd, SIZE),
				line->h_curr->cmd);
			line->max_size = ft_strlen(line->h_curr->cmd);
		}
		else
		{
			line->cmd = reset_cmd(line->cmd, SIZE);
			line->max_size = 0;
		}
		line->cursor = line->max_size;
	}
}

int				ft_hstr_lst(t_edit *line, t_hstr *new, int fd)
{
	t_hstr		*tmp;

	if (!line->h_first)
	{
		line->h_first = new;
		line->h_last = new;
		ft_dprintf(fd, "%s\n", new->cmd);
		return (0);
	}
	else if (ft_strcmp(new->cmd, line->h_last->cmd) != 0)
	{
		tmp = line->h_last;
		tmp->down = new;
		new->up = tmp;
		line->h_last = new;
		ft_dprintf(fd, "%s\n", new->cmd);
		return (0);
	}
	return (1);
}

int				ft_add_history(t_edit *line)
{
	t_hstr		*new;
	int			fd;

	if (line->cmd[0] == '\0')
		return (0);
	fd = ft_open_hstr();
	new_hstr(&new, line->cmd);
	if (ft_hstr_lst(line, new, fd))
	{
		free(new->cmd);
		free(new);
	}
	close(fd);
	return (0);
}
