/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:31:22 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/14 19:36:56 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char		*set_path(char *path)
{
	char	*buf;

	buf = NULL;
	if (path == NULL)
	{
		if ((buf = find_pattern("HOME")) != NULL)
			buf = ft_strdup(buf);
		else
			error_msg_m(4, "HOME", "cd");
	}
	else if (ft_strcmp(path, "-") == 0)
	{
		if ((buf = find_pattern("OLDPWD")) != NULL)
			buf = ft_strdup(buf);
		else
			error_msg_m(4, "OLDPWD", "cd");
	}
	else
		buf = fix_path(path);
	return (buf);
}

int			ft_cd(char **args)
{
	char	*buf;
	char	*oldpwd;
	int		ret;
	char	*path;

	if ((buf = set_path(args[1])) == NULL)
		return (1);
	if (args[1])
		path = buf;
	oldpwd = ft_strnew(SIZE);
	getcwd(oldpwd, SIZE);
	if (access(buf, F_OK) == 0 && access(buf, X_OK) != 0)
		ret = error_msg_m(5, path, "cd");
	else if ((ret = chdir(buf)) == 0)
	{
		set_env("OLDPWD", oldpwd);
		set_env("PWD", buf);
	}
	else
		ret = error_msg_m(1, path, "cd");
	if (buf)
		free(buf);
	free(oldpwd);
	return (ret);
}
