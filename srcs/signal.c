/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <cumberto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:20:56 by cumberto          #+#    #+#             */
/*   Updated: 2018/05/03 03:44:52 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void				handler_pid(int sig)
{
	g_ret = 1;
	kill(g_pid, sig);
	ft_printf("\n");
}

int					parent_handler(int sig)
{
	t_edit *line;

	sig = 0;
	line = get_line();
	end_key(line);
	write(1, "\n", 1);
	g_ret = 1;
	return (-1);
}

void				pipe_handler(int sig)
{
	if (sig == SIGPIPE)
	{
		write(1, "\n", 1);
	}
}

void				p_handler(int sig)
{
	sig = 0;
}

void				win_handler(int sig)
{
	struct winsize	win;

	if (sig == SIGWINCH)
	{
		ioctl(0, TIOCGWINSZ, &win);
		get_line()->col = win.ws_col;
		get_line()->row = win.ws_row;
	}
}
