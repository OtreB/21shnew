/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <cumberto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:20:56 by cumberto          #+#    #+#             */
/*   Updated: 2018/04/30 01:36:15 by tchapka          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

typedef struct 	s_dyna
{
	char *fname;
	struct s_dyna *next;
}				t_dyna;


char *curr_word(t_edit *line)
{
	int cnt;
	char *word;

	cnt = line->cursor;
	while(cnt > 0 && line->cmd[cnt] != ' ')
		cnt--;
	word = ft_strnew(line->cursor - cnt);
	word = ft_strncpy(word, &line->cursor[++cnt], line->cursor - cnt);
	return (word);
}

t_dyna	*new_dyn_elem(t_dyna *old, char *name)
{
	t_dyna *new;

	new = (*t_dyna)malloc(sizeof(t_dyna));
	new->fname = ft_strdup(name);
	new->next = NULL;
	if (old)
		old->next = new;
	else
		old = new;
	return (new);
}

int cmp_word(char *cword, char *cmplt)
{
	int cnt;

	while (cword[cnt] && cmplt[cnt] )
}


if (line->cursor == line->max_size && line->cursor > 0)
{
	char			path[SIZE];
	struct dirent	*direct;
	DIR				*pdir;
	t_dyna			*list;
	t_dyna			*first;
	char 			*cword;

	ft_bzero(path, sizeof(path));
	if (getcwd(path, SIZE - 1) == NULL)
		return (1);
	if ((pdir = opendir(path)) == NULL)
		return (1);

	first = NULL;
	list = NULL;

	while ((direct = readdir(pdir)) != NULL)
	{
		list = new_dyn_elem(list, direct->d_name);
		if (first == NULL)
			first = list;
	}

	cword = curr_word(line);

	tmp = first;
	while (tmp)
	{
		if (cmp_word(cword, tmp->fname))
		{
			complete_me()
			return ();
		}
		tmp = tmp->next;
	}

}

int					dyn_autocomp(t_edit *line)
{

}
