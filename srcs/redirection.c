/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:31:22 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/14 19:36:56 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int					custom_atoi(char *str, int *len)
{
	int				i;
	int				nb;

	i = 0;
	nb = 0;
	while (str[i] >= '0' && str[i] <= '9')
	{
		nb = (nb * 10) + (str[i] - 48);
		i++;
	}
	*len = i;
	return (nb);
}

int					make_dup(t_token *token)
{
	int				fd1;
	int				fd2;
	int				len;

	if (token->type == T_DUPO)
	{
		fd1 = custom_atoi(token->data, &len);
		if (token->data[len + 2] != '-')
			fd2 = custom_atoi(&token->data[len + 2], &len);
		else
			fd2 = open("/dev/null", O_WRONLY);
		if (fd1 < 0 || fd1 > 20 || fd2 < 0 || fd2 > 20)
			return (!!ft_dprintf(2, "error bad file descriptor\n"));
		dup2(fd2, fd1);
		return (0);
	}
	return (0);
}

int					make_red_in(t_token *token)
{
	int				fd;

	if (token->type == T_REDI || token->type == T_REDI_AP)
	{
		fd = open(token->data, O_RDONLY);
		dup2(fd, 0);
		close(fd);
		return (0);
	}
	return (0);
}

int					make_red_out(t_token *token)
{
	int				fd;

	if (token->type == T_REDO)
		fd = open(token->data, O_RDWR | O_CREAT | O_TRUNC, S_IRWXU);
	else if (token->type == T_REDO_AP)
		fd = open(token->data, O_RDWR | O_CREAT | O_APPEND, S_IRWXU);
	else
		return (0);
	dup2(fd, 1);
	close(fd);
	return (0);
}

int					make_redirection(t_token *token, unsigned int size)
{
	unsigned int	cnt;
	t_token			*tmp;

	cnt = 0;
	tmp = token;
	while (cnt < size && tmp)
	{
		if (tmp->type != T_WORD)
		{
			if (make_red_out(tmp) || make_red_in(tmp) || make_dup(tmp))
				return (1);
		}
		tmp = tmp->next;
		cnt++;
	}
	return (0);
}
