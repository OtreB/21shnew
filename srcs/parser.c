/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <cumberto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:20:56 by cumberto          #+#    #+#             */
/*   Updated: 2018/04/30 01:36:15 by tchapka          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char				**make_cmd(t_ast *node, unsigned int cnt, unsigned int ind)
{
	t_token			*tmp;
	char			**cmd;

	tmp = node->list;
	while (ind < node->tkn_num && tmp)
	{
		if (tmp->type == T_WORD || tmp->type == T_NUM)
			cnt++;
		ind++;
		tmp = tmp->next;
	}
	cmd = (char **)malloc(sizeof(char *) * (cnt + 1));
	cmd[cnt] = NULL;
	cnt = 0;
	ind = 0;
	tmp = node->list;
	while (ind < node->tkn_num && tmp)
	{
		if (tmp->type == T_WORD || tmp->type == T_NUM)
			cmd[cnt++] = tmp->data;
		ind++;
		tmp = tmp->next;
	}
	return (cmd);
}

int					check_redir(t_ast *node, unsigned int tmp_cnt)
{
	t_token			*tmp;
	t_token			*tmp2;

	tmp = node->list;
	while (tmp_cnt-- && tmp != NULL)
	{
		if (tmp->type == T_REDO || tmp->type == T_REDO_AP || tmp->type == T_REDI
			|| tmp->type == T_REDI_AP)
		{
			tmp2 = tmp->next;
			if (tmp2 && tmp2->type == T_WORD)
			{
				free(tmp->data);
				tmp->data = tmp2->data;
				tmp->next = tmp2->next;
				free(tmp2);
				node->tkn_num--;
			}
			else
				return (!!error_msg_m(7, tmp->data, EXE_NAME));
		}
		tmp = tmp->next;
	}
	return (0);
}

int					correct_gram(t_ast *node)
{
	while (node)
	{
		if (node->type == AST_CMD)
		{
			if (check_redir(node, node->tkn_num) == -1)
				return (-1);
			if (check_quote(node) == -1)
				return (-1);
			make_heredoc(node);
			node->cmd = make_cmd(node, 0, 0);
		}
		node = node->right;
	}
	return (0);
}

t_ast				*choose_node(t_ast *node, t_token *tmp, unsigned int *num)
{
	if (tmp->type >= T_REDI && *num == 0)
	{
		node = newnode(node);
		node->list = tmp;
		node->type = AST_CMD;
		*num = 1;
	}
	else if (tmp->type < T_REDI)
	{
		node = newnode(node);
		node->list = tmp;
		node->type = find_my_type(tmp->type);
		*num = 0;
	}
	return (node);
}

t_ast				*parse_token(t_token *token, t_ast *first)
{
	t_token			*tmp;
	t_ast			*node;
	unsigned int	num;

	tmp = token;
	node = NULL;
	first = NULL;
	num = 0;
	while (tmp)
	{
		node = choose_node(node, tmp, &num);
		if (first == NULL)
			first = node;
		node->tkn_num++;
		tmp = tmp->next;
	}
	if (correct_gram(first) == -1)
	{
		free_ast(first);
		return (NULL);
	}
	return (first);
}
