/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:31:22 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/14 19:13:15 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int					get_the_prompt(void)
{
	char			path[SIZE];
	unsigned int	cnt;
	int				tmp;
	wchar_t			wca;

	wca = L'࿓';
	cnt = 0;
	ft_bzero(path, sizeof(path));
	getcwd(path, SIZE - 1);
	cnt = ft_strlen(path);
	tmp = cnt;
	if (cnt == 0)
		return (0);
	while (path[cnt] != '/')
		cnt--;
	ft_printf("%s%lc%s  ", (g_ret ? RED : CYN), wca, RESET);
	ft_printf("%s%s%s\x1B[31m>\x1B[0m ", GRN, &path[cnt + 1], RESET);
	return (tmp - cnt + 4);
}

void				init_fd(void)
{
	get_fd()->in = dup(STDIN_FILENO);
	get_fd()->out = dup(STDIN_FILENO);
	get_fd()->err = dup(STDERR_FILENO);
}

void				reset_fd(void)
{
	dup2(get_fd()->in, STDIN_FILENO);
	dup2(get_fd()->out, STDOUT_FILENO);
	dup2(get_fd()->err, STDERR_FILENO);
}

t_token				*new_token(t_lexer *lexer)
{
	t_token			*new;

	new = (t_token *)malloc(sizeof(t_token));
	new->next = NULL;
	new->data = ft_strnew(lexer->size);
	new->type = T_NULL;
	lexer->cnt = 0;
	if (lexer->cur)
		lexer->cur->next = new;
	return (new);
}

t_lexer				init_lexer(t_lexer *lexer, int size)
{
	lexer->mod = M_GEN;
	lexer->cnt = 0;
	lexer->size = size;
	lexer->cur = NULL;
	lexer->first = new_token(lexer);
	lexer->cur = lexer->first;
	return (*lexer);
}
