/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <cumberto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:20:56 by cumberto          #+#    #+#             */
/*   Updated: 2018/04/30 01:36:15 by tchapka          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char			*trim_quote(char *str, char chr, int cnt, int cnt1)
{
	int			len;
	char		*new;

	len = ft_strlen(str) - 2;
	new = ft_strnew(len);
	while (str[cnt] && cnt1 < len)
	{
		if (str[cnt] != chr)
		{
			new[cnt1] = str[cnt];
			cnt1++;
			cnt++;
		}
		else
			cnt++;
	}
	new[cnt1] = '\0';
	free(str);
	return (new);
}

char			*read_quote(char *str, char ch)
{
	t_edit		line;
	char		*ret;

	ret = ft_strcat(copy_until(ft_strnew(SIZE), str, ch), "\n");
	line = *init_cmd_line(&line);
	while (ft_printf("dquote>"))
	{
		if (read_cmd_line(&line, 1, 7) == -1)
		{
			free(ret);
			free(line.cmd);
			return (NULL);
		}
		if (ft_strchrn(line.cmd, ch) >= 0)
		{
			line.cmd[ft_strchrn(line.cmd, ch)] = '\0';
			ft_strcat(ret, line.cmd);
			break ;
		}
		line.cmd[line.cursor++] = '\n';
		ret = ft_strcat(ret, line.cmd);
		reset_line(&line, 7);
	}
	free(line.cmd);
	return (ret);
}

int				d_quote(t_token *tmp)
{
	char		*tmps;
	int			ret;

	if ((ret = ft_strchrn(tmp->data, '\"')) < 0)
		return (0);
	if (ft_strchr(&tmp->data[ret + 1], '\"'))
		tmp->data = trim_quote(tmp->data, '\"', 0, 0);
	else
	{
		tmps = tmp->data;
		if ((tmp->data = read_quote(tmp->data, '\"')) == NULL)
		{
			tmp->data = tmps;
			return (-1);
		}
		free(tmps);
	}
	return (1);
}

int				s_quote(t_token *tmp)
{
	int			ret;
	char		*tmps;

	if ((ret = ft_strchrn(tmp->data, '\'')) < 0)
		return (0);
	if (ft_strchr(&tmp->data[ret + 1], '\''))
		tmp->data = trim_quote(tmp->data, '\'', 0, 0);
	else
	{
		tmps = tmp->data;
		if ((tmp->data = read_quote(tmp->data, '\'')) == NULL)
		{
			tmp->data = tmps;
			return (-1);
		}
		free(tmps);
	}
	return (1);
}

int				check_quote(t_ast *node)
{
	int			tmp_cnt;
	t_token		*tmp;
	int			ret;

	tmp = node->list;
	tmp_cnt = node->tkn_num;
	while (tmp && tmp_cnt--)
	{
		if ((ret = d_quote(tmp)) == 0 && ret != -1)
			ret = s_quote(tmp);
		if (ret == -1)
			return (-1);
		tmp = tmp->next;
	}
	return (0);
}
