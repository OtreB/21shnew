/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <cumberto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:31:22 by cumberto          #+#    #+#             */
/*   Updated: 2018/04/30 02:23:13 by tchapka          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void				ft_exit(char **args)
{
	int ret;

	if (args[1])
		ret = ft_atoi(args[1]);
	else
		ret = 0;
	clean_exit(ret);
}

t_edit				*init_cmd_line(t_edit *line)
{
	struct winsize	win;

	if ((line->cmd = ft_strnew(SIZE)) == NULL)
		exit(fatal_error(1));
	line->cursor = 0;
	line->max_size = 0;
	line->h_last = NULL;
	line->h_curr = NULL;
	line->h_first = NULL;
	ioctl(0, TIOCGWINSZ, &win);
	line->col = win.ws_col;
	line->row = win.ws_row;
	return (line);
}

int					make_stuff_happen(t_edit *line)
{
	t_token			*token;
	t_ast			*ast;
	unsigned int	boh;

	ast = NULL;
	g_pid = 0;
	if (line->max_size > 0)
	{
		if (lexer_analyzer(line->cmd, &token, line->max_size) == -1)
		{
			free_token(token);
			return (1);
		}
		if ((ast = parse_token(token, ast)) == NULL)
		{
			free_token(token);
			return (-1);
		}
		ast = make_ast(ast, NULL, 0);
		g_ret = ft_execute(ast, &boh);
		free_token(token);
		free_ast(ast);
		return (0);
	}
	return (1);
}

t_edit				*init_terminal(t_edit *line)
{
	signal(SIGINT, p_handler);
	signal(SIGPIPE, pipe_handler);
	signal(SIGWINCH, win_handler);
	init_fd();
	line = get_line();
	line = init_cmd_line(line);
	g_env = new_env();
	ft_init_hstr(line);
	return (line);
}

int					main(int argc, char *argv[])
{
	t_edit			*line;
	char			*name;

	(void)argv;
	(void)argc;
	name = getenv("TERM");
	if (!name || ft_strcmp(name, "xterm-256color"))
		exit(!!ft_dprintf(2, "couldn't set term attributes!\n"));
	line = NULL;
	line = init_terminal(line);
	ft_printf("Welcome to cumberto command interpreter!!!\n\nThis is a basic"
			" shell.\nType \"help\" to get more informations!\n\n");
	while (42)
	{
		if (read_cmd_line(line, 0, get_the_prompt()) != -1)
			make_stuff_happen(line);
	}
	clean_exit(0);
	return (0);
}
