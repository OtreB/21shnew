/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <cumberto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:20:56 by cumberto          #+#    #+#             */
/*   Updated: 2018/05/03 03:44:52 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		is_up_k(t_edit *line)
{
	if (line->cursor == line->max_size)
		ft_arrow_up(line);
	else if (line->cursor >= line->col)
	{
		ft_printf("\033[%dA", 1);
		line->cursor -= line->col;
	}
	if (line->cursor == line->max_size && (line->prompt_len + line->cursor)
		% line->col == 0)
	{
		tputs(tgetstr("do", NULL), 1, &putsomechar);
	}
	return (0);
}

int		is_do_k(t_edit *line)
{
	if (line->cursor == line->max_size)
		ft_arrow_down(line);
	else if (line->cursor + line->col <= line->max_size)
	{
		ft_printf("\033[%dB", 1);
		line->cursor += line->col;
	}
	if (line->cursor == line->max_size && (line->prompt_len + line->cursor)
		% line->col == 0)
	{
		tputs(tgetstr("do", NULL), 1, &putsomechar);
	}
	return (0);
}

int		is_ri_k(t_edit *line)
{
	if (line->cursor < line->max_size)
	{
		if ((line->cursor + line->prompt_len + 1) % line->col == 0)
			ft_printf("\033[%dG\033[%dB", 1, 1);
		else
			ft_printf("\033[%dC", 1);
		++line->cursor;
	}
	return (0);
}

int		is_le_k(t_edit *line)
{
	if (line->cursor != 0)
	{
		if ((line->cursor + line->prompt_len) % line->col == 0)
			ft_printf("\033[%dA\033[%dC", 1, line->col);
		else
			ft_printf("\033[%dD", 1);
		--line->cursor;
	}
	return (0);
}
