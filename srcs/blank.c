/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <cumberto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:20:56 by cumberto          #+#    #+#             */
/*   Updated: 2018/05/03 03:44:52 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int				putsomechar(int ch)
{
	int			fd;

	fd = open("/dev/tty", O_RDWR);
	write(fd, &ch, 1);
	close(fd);
	return (0);
}

int				ch_no_opt(t_edit *line, unsigned int opt, int ch)
{
	if (ch == K_RIGHT)
		return (is_ri_k(line));
	else if (ch == K_LEFT)
		return (is_le_k(line));
	else if (ch == K_HOME)
		return (home_key(line));
	else if (ch == K_END)
		return (end_key(line));
	else if (ch == SHIFT_LEFT)
		return (left_word(line));
	else if (ch == SHIFT_RIGHT)
		return (right_word(line));
	else if (ch == 127)
		return (deletechar(line));
	else if (ch == '\t' || ch == '\f')
		return (0);
	else if (ch >= 32 && ch < 127)
		return (printchar(line, ch));
	else if (ch == 3)
		return (parent_handler(SIGINT));
	else if (ch == 4)
		return (eof_handler(line, opt));
	return (0);
}

int				ft_read_char(t_edit *line, unsigned int opt, int ch)
{
	if (opt < 1)
	{
		if (ch == K_UP)
			return (is_up_k(line));
		else if (ch == K_DOWN)
			return (is_do_k(line));
		else if (ch == 20)
		{
			g_slctm = true;
			return (ft_select_mode(line));
		}
		else if (ch == 6)
			return (ft_paste(line));
	}
	if (ch == '\n')
	{
		end_key(line);
		write(1, &ch, 1);
		return (42);
	}
	return (ch_no_opt(line, opt, ch));
}

t_edit			*reset_line(t_edit *line, unsigned int prompt_len)
{
	line->cursor = 0;
	line->max_size = 0;
	line->clp_size = 0;
	line->h_curr = NULL;
	line->clp = NULL;
	line->prompt_len = prompt_len;
	reset_cmd(line->cmd, SIZE);
	return (line);
}

int				read_cmd_line(t_edit *line, int opt, unsigned int prompt_len)
{
	int			ch;
	int			ret;

	line = reset_line(line, prompt_len);
	set_key_env();
	while (42)
	{
		ch = 0;
		read(0, &ch, sizeof(int));
		ret = ft_read_char(line, opt, ch);
		if (ret == 42)
			break ;
		else if (ret == -1)
			return (-1);
		if (line->max_size > SIZE)
			return (!!ft_dprintf(2, "\ncommand too BIG\n"));
	}
	line->cmd[line->max_size] = '\0';
	if (line->clp)
		free(line->clp);
	if (opt == 0)
		ft_add_history(line);
	unset_key_env();
	return (0);
}
