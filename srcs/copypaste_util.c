/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:31:22 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/14 19:36:56 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int			reset_clp(t_edit *line)
{
	if (line->clp_size > 0)
	{
		free(line->clp);
		line->clp_size = 0;
		return (1);
	}
	return (0);
}

int			ctrl_c_m(t_edit *line, int cnt)
{
	g_slctm = false;
	line->cursor = cnt > (int)line->cursor ? line->cursor : cnt;
	end_key(line);
	write(1, "\n", 1);
	return (-1);
}

void		cursor_old_pos(t_edit *line, int size)
{
	int		row;
	int		col;

	col = (line->prompt_len + line->cursor + 1) % line->col;
	row = (col + size) / line->col;
	ft_printf("\033[%dG", col);
	if (row > 0)
		ft_printf("\033[%dA", row);
}

void		reset_line_cmd(t_edit *line)
{
	int		up;

	ft_printf("\033[%dG", line->prompt_len + 1);
	if ((up = (line->prompt_len + line->cursor) / line->col) > 0)
		ft_printf("\033[%dA", up);
	ft_printf("\033[J%s", line->cmd);
	line->cursor = line->max_size;
	if (((line->prompt_len + line->cursor) % line->col) == 0)
		tputs(tgetstr("do", NULL), 1, &putsomechar);
}

int			exit_clp(t_edit *line, int cnt, char buf)
{
	int		tmp;

	if (buf == 7)
	{
		tmp = line->cursor;
		line->clp_size = cnt > (int)line->cursor ? cnt - line->cursor
			: line->cursor - cnt;
		line->cursor = cnt > (int)line->cursor ? line->cursor : cnt;
		line->clp = ft_strndup(&line->cmd[line->cursor], line->clp_size);
		if (cnt > tmp)
			cursor_old_pos(line, line->clp_size);
		ft_cut(line);
	}
	else if (buf == 20 || buf == '\n')
	{
		line->clp_size = cnt > (int)line->cursor ? cnt - line->cursor :
			line->cursor - cnt;
		tmp = cnt > (int)line->cursor ? line->cursor : cnt;
		line->clp = ft_strndup(&line->cmd[tmp], line->clp_size);
		line->cursor = cnt;
	}
	return (0);
}
