/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <cumberto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:20:56 by cumberto          #+#    #+#             */
/*   Updated: 2018/04/30 01:36:15 by tchapka          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char				*ft_signal_str(int sig)
{
	if (sig == SIGABRT || sig == SIGIO)
		return ("abort");
	else if (sig == SIGBUS)
		return ("bus error");
	else if (sig == SIGSEGV)
		return ("segmentation fault");
	else
		return ("unknown error");
}

void				clean_exit(int ret)
{
	ft_free_history(get_line());
	free(get_line()->cmd);
	if (get_line()->clp_size)
		free(get_line()->clp);
	free_table(g_env);
	unset_key_env();
	exit(ret);
}

char				*copy_until(char *dest, char *src, char ch)
{
	int				cnt1;
	int				cnt2;

	cnt1 = 0;
	cnt2 = 0;
	while (src[cnt2])
	{
		if (src[cnt2] != ch)
		{
			dest[cnt1] = src[cnt2];
			cnt1++;
			cnt2++;
		}
		else
			cnt2++;
	}
	return (dest);
}

int					ft_strchrn(char *str, char ch)
{
	int				cnt;

	cnt = 0;
	while (str[cnt] != '\0')
	{
		if (str[cnt] == ch)
			return (cnt);
		cnt++;
	}
	return (-1);
}
