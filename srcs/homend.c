/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <cumberto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:20:56 by cumberto          #+#    #+#             */
/*   Updated: 2018/05/03 03:44:52 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int			end_key(t_edit *line)
{
	int		up;
	int		start;

	if (line->cursor < line->max_size)
	{
		start = ((line->prompt_len + line->max_size + 1) % line->col);
		ft_printf("\033[%dG", start == 0 ? line->col : start);
		start = ((line->prompt_len + line->cursor) % line->col);
		if ((up = ((start + line->max_size - line->cursor) / line->col)) > 0)
			CURSOR_DOWN(up);
		line->cursor = line->max_size;
	}
	return (0);
}

int			home_key(t_edit *line)
{
	int		up;

	if (line->cursor <= line->max_size)
	{
		ft_printf("\033[%dG", line->prompt_len + 1);
		up = (line->prompt_len + line->cursor) / line->col;
		if (up > 0)
			CURSOR_UP(up);
		line->cursor = 0;
	}
	return (0);
}
