/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <cumberto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:20:56 by cumberto          #+#    #+#             */
/*   Updated: 2018/05/03 03:44:52 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int				print_normal(t_edit *line, char ch)
{
	write(1, &ch, 1);
	++line->cursor;
	line->cmd[line->max_size] = ch;
	++line->max_size;
	if (((line->prompt_len + line->cursor) % line->col) == 0)
		tputs(tgetstr("do", NULL), 1, &putsomechar);
	return (0);
}

int				printchar(t_edit *line, char ch)
{
	char		*tmp;
	int			start;
	int			up;

	start = ((line->prompt_len + line->cursor + 1) % line->col);
	if (line->cursor < line->max_size)
	{
		tmp = ft_strdup(&line->cmd[line->cursor]);
		line->cmd[line->cursor] = ch;
		line->cursor = line->cursor + 1;
		line->cmd[line->cursor] = '\0';
		line->cmd = ft_strcat(line->cmd, tmp);
		ft_printf("%c%s\033[%dG", ch, tmp, start + 1);
		if ((up = (start + line->max_size - line->cursor) / line->col) > 0)
			ft_printf("\033[%dA", up);
		free(tmp);
		++line->max_size;
	}
	else
		print_normal(line, ch);
	return (0);
}

int				left_word(t_edit *line)
{
	if (line->cmd[line->cursor] == ' ')
	{
		while (line->cursor > 0 && line->cmd[line->cursor] == ' ')
			is_le_k(line);
	}
	while (line->cursor > 0 && line->cmd[line->cursor] != ' ')
		is_le_k(line);
	return (0);
}

int				right_word(t_edit *line)
{
	if (line->cmd[line->cursor] == ' ')
	{
		while (line->cursor < line->max_size && line->cmd[line->cursor] == ' ')
			is_ri_k(line);
	}
	while (line->cursor < line->max_size && line->cmd[line->cursor] != ' ')
		is_ri_k(line);
	return (0);
}

int				eof_handler(t_edit *line, int opt)
{
	if (opt == 0 && line->max_size == 0)
		clean_exit(0);
	else if (opt == 1)
	{
		write(1, "\n", 1);
		return (3);
	}
	return (0);
}
