/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:31:22 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/14 16:51:56 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include <termios.h>

char				**cp_env(void)
{
	char			**newenv;
	unsigned int	cnt;

	cnt = 0;
	newenv = (char**)malloc(sizeof(char*) * 100);
	while (g_env[cnt] != NULL)
	{
		newenv[cnt] = ft_strdup(g_env[cnt]);
		cnt++;
	}
	newenv[cnt] = NULL;
	return (newenv);
}

int					print_env(char **env)
{
	unsigned int	ind;

	ind = 0;
	while (env[ind])
	{
		ft_printf("%s\n", env[ind]);
		ind++;
	}
	return (0);
}

void				set_key_env(void)
{
	const char		*name;
	struct termios	term;

	name = getenv("TERM");
	if (!name || ft_strcmp(name, "xterm-256color"))
		ft_dprintf(2, "couldn't set term attributes!\n");
	else if (tgetent(NULL, name) == ERR)
		ft_dprintf(2, "couldn't set term attributes!\n");
	else if (tcgetattr(0, &term) == -1)
		ft_dprintf(2, "couldn't set term attributes!\n");
	term.c_lflag &= ~(ICANON | ECHO);
	term.c_cc[VMIN] = 1;
	term.c_cc[VEOF] = 4;
	term.c_cc[VINTR] = 2;
	if (tcsetattr(0, TCSADRAIN, &term) == -1)
		ft_dprintf(2, "couldn't set term attributes!\n");
}

void				unset_key_env(void)
{
	const char		*name;
	struct termios	term;

	if (!(name = getenv("TERM")))
		ft_dprintf(2, "couldn't set term attributes!\n");
	else if (tgetent(NULL, name) <= 0)
		ft_dprintf(2, "couldn't set term attributes!\n");
	else if (tcgetattr(0, &term) == -1)
		ft_dprintf(2, "couldn't set term attributes!\n");
	term.c_lflag |= (ECHO);
	term.c_lflag |= (ICANON);
	term.c_lflag &= ~(ECHOCTL);
	term.c_cc[VMIN] = 1;
	term.c_cc[VEOF] = 4;
	term.c_cc[VINTR] = 3;
	if (tcsetattr(0, TCSADRAIN, &term) == -1)
		ft_dprintf(2, "couldn't set term attributes!\n");
}
