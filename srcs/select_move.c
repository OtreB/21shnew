/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:31:22 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/14 16:51:56 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <minishell.h>

int					ft_select_right(t_edit *line, int cnt)
{
	char			*offm;
	char			*revm;

	if (cnt < (int)line->max_size)
	{
		revm = cnt >= (int)line->cursor ? "\033[7m" : NULL;
		offm = cnt >= (int)line->cursor ? "\033[0m" : NULL;
		if ((cnt + line->prompt_len) % line->col == 0)
			ft_printf("\033[%dG\033[%dB", 0, 1);
		ft_printf("%s%c%s", revm, line->cmd[cnt], offm);
		++cnt;
		return (cnt);
	}
	return (cnt);
}

int					ft_select_left(t_edit *line, int cnt)
{
	char			*revm;
	char			*offm;

	if (cnt > 0)
	{
		revm = cnt <= (int)line->cursor ? "\033[7m" : NULL;
		offm = cnt <= (int)line->cursor ? "\033[0m" : NULL;
		--cnt;
		if (cnt >= 0)
		{
			if ((cnt + line->prompt_len + 1) % line->col == 0)
				ft_printf("\033[%dG\033[%dC\033[%dA%s%c%s", line->col,
					1, 1, revm, line->cmd[cnt], offm);
			else
				ft_printf("\b%s%c%s\b", revm, line->cmd[cnt], offm);
		}
	}
	return (cnt);
}

int					ft_select_up(t_edit *line, int cnt)
{
	unsigned int	num;

	num = 0;
	if (cnt - line->col != 0)
	{
		while (num < line->col)
		{
			cnt = ft_select_left(line, cnt);
			num++;
		}
	}
	return (cnt);
}

int					ft_select_down(t_edit *line, int cnt)
{
	unsigned int	num;

	num = 0;
	if (cnt + line->col <= line->max_size)
	{
		while (num < line->col)
		{
			cnt = ft_select_right(line, cnt);
			num++;
		}
	}
	return (cnt);
}
