/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:31:22 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/14 16:51:56 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void			read_token2(char chr, t_tokenlist *token)
{
	if (chr == '&')
	{
		if (*token == T_NULL)
			*token = T_AMP;
		else if (*token == T_AMP)
			*token = T_DAMP;
		else if (*token == T_REDI || *token == T_REDO)
			*token = *token == T_REDO ? T_DUPO : T_DUPI;
		else
			*token = T_ERRTKN;
	}
	if (chr == ';')
	{
		if (*token == T_NULL)
			*token = T_SEMIC;
		else
			*token = T_ERRTKN;
	}
}

void			read_token1(char chr, t_tokenlist *token)
{
	if (chr == '>' || chr == '<')
	{
		if (*token == T_NULL || *token == T_NUM)
			*token = chr == '>' ? T_REDO : T_REDI;
		else if (*token == T_REDO && chr == '>')
			*token = T_REDO_AP;
		else if (*token == T_REDI && chr == '<')
			*token = T_REDI_AP;
		else
			*token = T_ERRTKN;
	}
	if (chr == '|')
	{
		if (*token == T_NULL)
			*token = T_PIPE;
		else if (*token == T_PIPE)
			*token = T_DPIPE;
		else
			*token = T_ERRTKN;
	}
}

int				chr_is_token(t_lexer *lexer, char chr)
{
	char		*metach;

	metach = "<>|&;\n";
	if (!lexer->mod && chr == '-' && (lexer->cur->type == T_DUPI
		|| lexer->cur->type == T_DUPO))
	{
		lexer->cur->data[lexer->cnt] = chr;
		lexer->cur = new_token(lexer);
		return (1);
	}
	if (!lexer->mod && ft_strchr(metach, chr))
	{
		if (lexer->cur->type == T_WORD)
			lexer->cur = new_token(lexer);
		read_token1(chr, &lexer->cur->type);
		read_token2(chr, &lexer->cur->type);
		lexer->cur->data[lexer->cnt] = chr;
		if (lexer->cur->type == T_ERRTKN)
			return (error_msg_m(6, lexer->cur->data, EXE_NAME) * 0 - 1);
		lexer->cnt++;
		return (1);
	}
	return (0);
}
