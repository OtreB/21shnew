/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:31:22 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/14 19:36:56 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int				ft_wait_direct_cmd(pid_t pid, char **av)
{
	int	res;

	g_pid = pid;
	signal(SIGINT, handler_pid);
	if (waitpid(pid, &res, 0) == -1)
		return (error_bad(1, "21sh", "waitpid error"));
	ft_exit_code(&res, av[0]);
	signal(SIGINT, p_handler);
	return (res);
}
