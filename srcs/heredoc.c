/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:31:22 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/14 18:08:41 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int					ft_heredoc(char *limit, int fd)
{
	t_edit			line;
	int				ret;

	line = *init_cmd_line(&line);
	line.prompt_len = 3;
	while (42)
	{
		ft_printf(">> ");
		if ((ret = read_cmd_line(&line, 1, 3)) == -1)
		{
			free(line.cmd);
			return (-1);
		}
		if (ft_strcmp(line.cmd, limit) == 0 || ret == 3)
		{
			ft_strdel(&line.cmd);
			break ;
		}
		ft_dprintf(fd, "%s\n", line.cmd);
		reset_line(&line, 3);
	}
	close(fd);
	if (line.cmd)
		ft_strdel(&line.cmd);
	return (0);
}

int					open_heredoc(t_token *token, char *limit)
{
	int				fd;

	token->data = ft_strnew(24);
	ft_strcpy(token->data, "/tmp/heredoc-");
	ft_strcat(token->data, limit);
	if ((fd = open(token->data, O_WRONLY | O_CREAT | O_TRUNC,
		S_IRUSR | S_IWUSR)) < 0)
		return (-1);
	return (fd);
}

void				make_heredoc(t_ast *node)
{
	t_token			*tmp;
	char			*limit;
	unsigned int	cnt;
	int				fd;

	cnt = 0;
	tmp = node->list;
	while (cnt < node->tkn_num)
	{
		if (tmp->type == T_REDI_AP)
		{
			limit = tmp->data;
			fd = open_heredoc(tmp, limit);
			ft_heredoc(limit, fd);
			free(limit);
		}
		tmp = tmp->next;
		cnt++;
	}
}
