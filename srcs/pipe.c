/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:31:22 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/14 19:36:56 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void		ft_redir_fd(int old, int new)
{
	if (old == new)
		return ;
	if (dup2(old, new) != -1)
		close(old);
}

int			ft_pipe_fork(t_ast *ast, int input, unsigned int *count)
{
	pid_t	pid;
	int		pfd[2];
	int		st;

	pipe(pfd);
	if ((pid = fork()) == 0)
	{
		close(pfd[0]);
		close(input);
		return (ft_exec_pipe(ast->left, pfd[1], count));
	}
	else if (pid == -1)
		return (1);
	else
	{
		while (waitpid(pid, &st, WNOHANG) == -1)
			continue;
		close(pfd[1]);
		ft_redir_fd(pfd[0], STDIN_FILENO);
		ft_redir_fd(input, STDOUT_FILENO);
		exit(ft_eval_cmd(ast->right, count));
		return (st);
	}
}

int			ft_exec_pipe(t_ast *ast, int input, unsigned int *count)
{
	if (ast->left == NULL)
	{
		ft_redir_fd(input, STDOUT_FILENO);
		exit(ft_eval_cmd(ast, count));
	}
	else
		return (ft_pipe_fork(ast, input, count));
	return (0);
}

int			ft_pipe_cmd(t_ast *ast, unsigned int *count)
{
	int		status;
	pid_t	pid;

	if ((pid = fork()) == 0)
		exit(ft_exec_pipe(ast, STDOUT_FILENO, count));
	else if (pid == -1)
		return (1);
	else
	{
		g_pid = pid;
		if (waitpid(pid, &status, 0) == -1)
			return (1);
		ft_exit_code(&status, "pipe");
		return (status);
	}
	return (0);
}
