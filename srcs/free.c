/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/07 15:12:36 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/11 15:27:29 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void				ft_free_history(t_edit *line)
{
	t_hstr			*tmp1;
	t_hstr			*tmp2;

	tmp1 = line->h_first;
	while (tmp1)
	{
		tmp2 = tmp1;
		tmp1 = tmp1->down;
		free(tmp2->cmd);
		free(tmp2);
	}
}

void				free_ast(t_ast *ast)
{
	if (ast == NULL)
		return ;
	free_ast(ast->left);
	free_ast(ast->right);
	free(ast->cmd);
	free(ast);
}

void				free_token(t_token *token)
{
	t_token *tmp;

	while (token)
	{
		free(token->data);
		tmp = token->next;
		free(token);
		token = tmp;
	}
}

void				free_table(char **table)
{
	unsigned int	cnt;

	if (table != NULL)
	{
		cnt = 0;
		while (table[cnt] != NULL)
		{
			free(table[cnt]);
			cnt++;
		}
		free(table);
	}
}
