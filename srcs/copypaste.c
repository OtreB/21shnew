/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:31:22 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/14 19:36:56 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void		is_valid_clp(t_edit *line, int cnt, char buf)
{
	if (cnt - line->cursor == 0)
	{
		line->clp = NULL;
		line->clp_size = 0;
		line->cursor = cnt;
		g_slctm = false;
		return ;
	}
	exit_clp(line, cnt, buf);
	g_slctm = false;
	return ;
}

int			ft_select_mode(t_edit *line)
{
	int		buf;
	int		cnt;

	reset_clp(line);
	cnt = line->cursor;
	buf = 0;
	while (buf != 20 && buf != 7 && buf != '\n' && g_slctm)
	{
		buf = 0;
		read(1, &buf, sizeof(int));
		if (buf == K_RIGHT)
			cnt = ft_select_right(line, cnt);
		else if (buf == K_LEFT)
			cnt = ft_select_left(line, cnt);
		else if (buf == K_UP)
			cnt = ft_select_up(line, cnt);
		else if (buf == K_DOWN)
			cnt = ft_select_down(line, cnt);
		else if (buf == 3)
			return (ctrl_c_m(line, cnt));
	}
	is_valid_clp(line, cnt, buf);
	reset_line_cmd(line);
	return (0);
}

int			ft_cut(t_edit *line)
{
	char	*tmp;

	line->cmd[line->cursor] = '\0';
	tmp = ft_strdup(&line->cmd[line->cursor + line->clp_size]);
	ft_strcat(line->cmd, tmp);
	line->max_size -= line->clp_size;
	line->cmd[line->max_size] = '\0';
	ft_printf("%s\033[J", tmp);
	cursor_old_pos(line, ft_strlen(tmp));
	free(tmp);
	return (0);
}

int			paste_here(t_edit *line)
{
	char	*tmp;

	if (line->cursor < line->max_size)
	{
		tmp = ft_strdup(&line->cmd[line->cursor]);
		line->cmd[line->cursor] = '\0';
		ft_strcat(ft_strncat(line->cmd, line->clp, line->clp_size), tmp);
		ft_printf("%s%s", line->clp, tmp);
		free(tmp);
	}
	else
	{
		ft_printf("%s", line->clp);
		ft_strcat(line->cmd, line->clp);
	}
	line->max_size += line->clp_size;
	line->cursor = line->max_size;
	if (((line->prompt_len + line->cursor) % line->col) == 0)
		tputs(tgetstr("do", NULL), 1, &putsomechar);
	return (0);
}

int			ft_paste(t_edit *line)
{
	int		cp;

	cp = line->cursor == 0 ? 0 : 1;
	if (line->max_size + line->clp_size > SIZE)
	{
		ft_dprintf(2, "\ncommand too BIG\n");
		line = reset_line(line, 0);
		get_the_prompt();
		return (-1);
	}
	if (line->clp_size != 0)
		paste_here(line);
	return (0);
}
