/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   history.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <cumberto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:31:22 by cumberto          #+#    #+#             */
/*   Updated: 2018/04/30 02:35:56 by tchapka          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char			*reset_cmd(char *str, int len)
{
	int		cnt;

	cnt = 0;
	while (cnt < len)
	{
		str[cnt] = '\0';
		cnt++;
	}
	return (str);
}

int				ft_open_hstr(void)
{
	int		fd;
	char	*tmp;
	char	*path;

	tmp = ft_strdup(find_pattern("HOME"));
	path = ft_strcpy(ft_strnew(ft_strlen(tmp) + 14), tmp);
	path = ft_strcat(ft_strcat(path, "/"), HSTR);
	fd = open(path, O_RDWR | O_APPEND | O_CREAT, S_IRWXU);
	free(tmp);
	free(path);
	if (fd == -1)
		exit(fatal_error(1));
	return (fd);
}

int				ft_print_history(t_edit *line)
{
	t_hstr	*tmp;
	int		cnt;

	cnt = 0;
	tmp = line->h_first;
	while (tmp->down)
	{
		ft_printf("%d\t%s\n", cnt, tmp->cmd);
		cnt++;
		tmp = tmp->down;
	}
	return (0);
}

int				new_hstr(t_hstr **nw, char *str)
{
	t_hstr		*new;

	if ((new = ft_memalloc(sizeof(t_hstr))) == NULL)
		exit(fatal_error(1));
	new->cmd = ft_strdup(str);
	new->up = NULL;
	new->down = NULL;
	*nw = new;
	return (0);
}

void			ft_init_hstr(t_edit *line)
{
	int			fd;
	char		*buf;
	t_hstr		*tmp;
	t_hstr		*new;

	fd = ft_open_hstr();
	while (ft_get_next_line(fd, &buf))
	{
		new_hstr(&new, buf);
		if (!line->h_first)
		{
			line->h_first = new;
			line->h_last = new;
		}
		else
		{
			tmp = line->h_last;
			tmp->down = new;
			new->up = tmp;
			line->h_last = new;
		}
		free(buf);
	}
	close(fd);
}
