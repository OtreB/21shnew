/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:31:22 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/14 17:45:20 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_astp			find_my_type(t_tokenlist type)
{
	if (type == T_SEMIC)
		return (AST_SEMIC);
	else if (type == T_DAMP)
		return (AST_DAMP);
	else if (type == T_DPIPE)
		return (AST_DPIPE);
	else if (type == T_PIPE)
		return (AST_PIPE);
	else
		return (AST_NULL);
}

t_ast			*newnode(t_ast *list)
{
	t_ast		*node;

	node = (t_ast *)malloc(sizeof(t_ast));
	if (list == NULL)
		list = node;
	else
		list->right = node;
	node->list = NULL;
	node->type = AST_NULL;
	node->tkn_num = 0;
	node->right = NULL;
	node->left = NULL;
	node->cmd = NULL;
	return (node);
}

t_ast			*make_ast(t_ast *token, t_ast *delim, t_tokenlist prio)
{
	t_ast		*tmp;
	t_ast		*save;
	t_ast		*node;

	save = NULL;
	tmp = token;
	if (prio == T_ERRTKN || !token)
		return (NULL);
	while (tmp != delim)
	{
		if (tmp->list->type == prio)
			save = tmp;
		tmp = tmp->right;
	}
	if (save)
	{
		node = save;
		node->left = make_ast(token, save, prio);
		node->right = make_ast(save->right, delim, prio + 1);
		return (node);
	}
	else
		return (make_ast(token, delim, prio + 1));
}
