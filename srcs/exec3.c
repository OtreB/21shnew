/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:31:22 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/14 19:36:56 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void			ft_exit_code(int *res, char *name)
{
	int	i;

	if (WIFEXITED(*res))
		*res = WEXITSTATUS(*res);
	else if (WIFSIGNALED(*res))
	{
		i = WTERMSIG(*res);
		if (i != SIGINT && i != SIGPIPE)
			ft_dprintf(2, "21sh : %s: %s %d\n", name, ft_signal_str(i), i);
	}
	else if (WIFSTOPPED(*res))
	{
		i = WSTOPSIG(*res);
		ft_dprintf(2, "21sh : %s: %s\n", name, ft_signal_str(i), i);
	}
	g_ret = *res;
}

int				exec_cmd(char **av, t_ast *ast, unsigned int *count)
{
	pid_t	pid;
	char	*path;

	count = 0;
	if ((path = check_exec(av[0])) == NULL)
		return (1);
	if ((pid = fork()) == 0)
	{
		if (make_redirection(ast->list, ast->tkn_num))
			exit(EXIT_FAILURE);
		if (execve(path, av, g_env))
			return (error_bad(1, "21sh", "execve failed"));
	}
	else if (pid == -1)
		return (error_bad(1, "21sh", "fork error"));
	free(path);
	return (ft_wait_direct_cmd(pid, av));
}

int				ft_eval_cmd(t_ast *node, unsigned int *count)
{
	if (check_if_builtin(node->cmd[0]) == 0)
		return (exec_builtin(node->cmd, node));
	else
		return (exec_cmd(node->cmd, node, count));
}

static int		ft_eval_op(t_ast *a, t_astp type, unsigned int *c)
{
	if (AST_PIPE == type)
		return (ft_pipe_cmd(a, c));
	else if (AST_DPIPE == type)
	{
		return (ft_execute(a->left, c) == 0\
			|| ft_execute(a->right, c) == 0);
	}
	else if (AST_DAMP == type)
	{
		return (ft_execute(a->left, c) == 0\
			&& ft_execute(a->right, c) == 0);
	}
	else if (AST_SEMIC == type)
	{
		ft_execute(a->left, c);
		return (ft_execute(a->right, c));
	}
	return (0);
}

int				ft_execute(t_ast *node, unsigned int *count)
{
	if (node == NULL)
		return (0);
	if (node->type == AST_CMD)
		return (ft_eval_cmd(node, count));
	else
		return (ft_eval_op(node, node->type, count));
	return (0);
}
