/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   history.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <cumberto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:31:22 by cumberto          #+#    #+#             */
/*   Updated: 2018/04/30 02:35:56 by tchapka          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void			delete_mid(t_edit *line)
{
	char		*tmp;

	tmp = ft_strdup(&line->cmd[line->cursor]);
	line->cmd[line->cursor - 1] = '\0';
	line->cmd = ft_strcat(line->cmd, tmp);
	if ((line->cursor + line->prompt_len) % line->col)
		ft_printf("\b");
	else
		ft_printf("\033[%dA\033[%dG", 1, line->col);
	ft_printf("%s%s %s", CURSOR_SAVE, tmp, CURSOR_REST);
	free(tmp);
}

int				deletechar(t_edit *line)
{
	if (line->cursor > 0)
	{
		if (line->cursor < line->max_size)
			delete_mid(line);
		else
		{
			if ((line->cursor + line->prompt_len) % line->col == 0)
				ft_printf("\033[%dA\033[%dG \033[%dG", 1, line->col, line->col);
			else
				ft_printf("\b \b");
			line->cmd[line->cursor - 1] = '\0';
		}
		--line->cursor;
		--line->max_size;
	}
	return (0);
}
