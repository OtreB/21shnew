/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setenv.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/08 15:27:15 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/14 17:57:19 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int					env_name_cmp(char *env, char *name)
{
	unsigned int	ind;

	ind = 0;
	while (env[ind] && name[ind] && env[ind] == name[ind] && env[ind] != '=')
		ind++;
	if (env[ind] == '=')
		return (0);
	return (1);
}

char				*find_pattern(char *pattern)
{
	char			**env;
	unsigned int	cnt;

	cnt = 0;
	env = g_env;
	if (env != NULL)
	{
		while (env[cnt] != NULL)
		{
			if (env_name_cmp(env[cnt], pattern) == 0)
				return (ft_strchr(env[cnt], '=') + 1);
			cnt++;
		}
	}
	return (NULL);
}

char				*set_env(char *name, char *value)
{
	unsigned int	cnt;
	char			*buf;

	cnt = 0;
	buf = ft_strnew(ft_strlen(name) + ft_strlen(value) + 1);
	buf = ft_strcat(ft_strcat(ft_strcat(buf, name), "="), value);
	while (g_env[cnt])
	{
		if (env_name_cmp(g_env[cnt], name) == 0)
		{
			free(g_env[cnt]);
			g_env[cnt] = buf;
			return (value);
		}
		cnt++;
	}
	g_env[cnt] = buf;
	cnt++;
	g_env[cnt] = NULL;
	return (value);
}

int					ft_unsetenv(char **args)
{
	char			**newenv;
	unsigned int	cnt;
	unsigned int	n_cnt;
	char			*name;

	name = args[1];
	if (name == NULL)
		return (1);
	cnt = 0;
	n_cnt = 0;
	newenv = (char**)malloc(sizeof(char*) * 100);
	while (g_env[cnt] != NULL)
	{
		if (env_name_cmp(g_env[cnt], name))
		{
			newenv[n_cnt] = ft_strdup(g_env[cnt]);
			n_cnt++;
		}
		cnt++;
	}
	newenv[n_cnt] = NULL;
	free_table(g_env);
	g_env = newenv;
	return (0);
}

int					ft_setenv(char **arg)
{
	if (arg[1] != NULL)
		if (arg[2] != NULL)
			if (arg[3] == NULL)
			{
				if (ft_strchr(arg[0], '=') == NULL)
				{
					set_env(arg[1], arg[2]);
					return (0);
				}
			}
	ft_dprintf(2, "setenv: wrong usage: [name] [value]\n", 37);
	return (1);
}
