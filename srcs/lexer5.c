/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:31:22 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/14 16:51:56 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int				chr_is_gen(t_lexer *lexer, char chr)
{
	if (!lexer->mod && (chr == ' ' || chr == '\t'))
	{
		if (lexer->cur->type != T_NULL)
			lexer->cur = new_token(lexer);
		return (0);
	}
	if (lexer->cur->type != T_WORD && lexer->cur->type != T_NULL)
		lexer->cur = new_token(lexer);
	lexer->cur->type = T_WORD;
	lexer->cur->data[lexer->cnt] = chr;
	lexer->cnt++;
	return (0);
}

int				chr_is_digit(t_lexer *lexer, char chr)
{
	if (!lexer->mod && ft_isdigit(chr))
	{
		lexer->cur->type = lexer->cur->type == T_NULL ? T_NUM :
			lexer->cur->type;
		lexer->cur->data[lexer->cnt] = chr;
		lexer->cnt++;
		return (1);
	}
	return (0);
}

int				chr_is_quote(t_lexer *lexer, char chr)
{
	if (chr == '\"' && lexer->mod != M_QUOT)
	{
		if (lexer->cur->type != T_WORD && lexer->cur->type != T_NULL)
			lexer->cur = new_token(lexer);
		lexer->mod = lexer->mod == M_DQUOT ? M_GEN : M_DQUOT;
		lexer->cur->data[lexer->cnt] = chr;
		lexer->cnt++;
		lexer->cur->type = T_WORD;
		return (1);
	}
	else if (chr == '\'' && lexer->mod != M_DQUOT)
	{
		if (lexer->cur->type != T_WORD && lexer->cur->type != T_NULL)
			lexer->cur = new_token(lexer);
		lexer->mod = lexer->mod == M_QUOT ? M_GEN : M_QUOT;
		lexer->cur->data[lexer->cnt] = chr;
		lexer->cnt++;
		lexer->cur->type = T_WORD;
		return (1);
	}
	return (0);
}

int				end_quote(int ch)
{
	int			res;
	char		*line;

	unset_key_env();
	ft_printf(">> ");
	while ((res = ft_get_next_line(1, &line)) > 0 && g_slctm)
	{
		if (ft_strchr(line, ch))
			break ;
		ft_printf(">> ");
		ft_printf("%s\n", line);
		ft_strdel(&line);
	}
	g_slctm = false;
	set_key_env();
	if (res < 0)
		return (!!ft_dprintf(2, "21sh : heredoc : error gnl\n"));
	if (line)
		ft_strdel(&line);
	return (0);
}

int				lexer_analyzer(char *cmd, t_token **token, int size)
{
	t_lexer		lexer;
	int			ind;
	int			tkn;
	int			tmps;

	lexer = init_lexer(&lexer, size);
	*token = lexer.first;
	ind = 0;
	tmps = lexer.size;
	while (cmd[ind] && ind < tmps)
	{
		if ((tkn = chr_is_token(&lexer, cmd[ind])) == -1)
			return (tkn);
		if (!tkn)
		{
			chr_is_digit(&lexer, cmd[ind]) ||
			chr_is_quote(&lexer, cmd[ind]) ||
			chr_is_gen(&lexer, cmd[ind]);
		}
		ind++;
		lexer.size--;
	}
	return (0);
}
