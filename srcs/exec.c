/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:31:22 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/14 19:13:15 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

bool				exec_builtin(char **cmd, t_ast *ast)
{
	if (make_redirection(ast->list, ast->tkn_num))
		exit(EXIT_FAILURE);
	if (ft_strcmp(cmd[0], "cd") == 0)
		g_ret = ft_cd(cmd);
	else if (ft_strcmp(cmd[0], "exit") == 0)
		ft_exit(cmd);
	else if (ft_strcmp(cmd[0], "env") == 0)
		g_ret = ft_env(cmd, ast);
	else if (ft_strcmp(cmd[0], "setenv") == 0)
		g_ret = ft_setenv(cmd);
	else if (ft_strcmp(cmd[0], "unsetenv") == 0)
		g_ret = ft_unsetenv(cmd);
	else if (ft_strcmp(cmd[0], "echo") == 0)
		g_ret = ft_echo(cmd);
	else if (ft_strcmp(cmd[0], "history") == 0)
		g_ret = ft_print_history(get_line());
	else
	{
		reset_fd();
		return (true);
	}
	reset_fd();
	return (false);
}

int					check_if_builtin(char *cmd)
{
	if (ft_strcmp(cmd, "cd") == 0)
		return (0);
	else if (ft_strcmp(cmd, "exit") == 0)
		return (0);
	else if (ft_strcmp(cmd, "env") == 0)
		return (0);
	else if (ft_strcmp(cmd, "setenv") == 0)
		return (0);
	else if (ft_strcmp(cmd, "unsetenv") == 0)
		return (0);
	else if (ft_strcmp(cmd, "echo") == 0)
		return (0);
	else if (ft_strcmp(cmd, "history") == 0)
		return (0);
	else
		return (1);
}

int					is_regular(char *path)
{
	struct stat		reg;

	stat(path, &reg);
	return (S_ISDIR(reg.st_mode));
}

char				*select_path(char *cmd)
{
	extern char		**environ;
	unsigned int	cnt;
	char			**path;
	char			*tmp;

	cnt = 0;
	if ((path = ft_strsplit(find_pattern("PATH"), ':')) == NULL)
		return (NULL);
	tmp = ft_strnew(PATH_SIZE);
	while (path[cnt])
	{
		ft_bzero(tmp, sizeof(tmp));
		tmp = ft_strcat(ft_strcat(ft_strcat(tmp, path[cnt]), "/"), cmd);
		if (access(tmp, F_OK) == 0)
			break ;
		cnt++;
	}
	if (path[cnt] == NULL)
	{
		free(tmp);
		tmp = NULL;
	}
	free_table(path);
	return (tmp);
}

char				*check_exec(char *cmd)
{
	char			*path;

	if (ft_strchr(cmd, '/'))
		path = fix_path(cmd);
	else
		path = select_path(cmd);
	if (path == NULL || access(path, F_OK) != 0 || is_regular(path) == 1)
	{
		if (path != NULL)
			free(path);
		error_msg_m(3, cmd, EXE_NAME);
		return (NULL);
	}
	if (access(path, X_OK) != 0)
	{
		free(path);
		error_msg_m(5, cmd, EXE_NAME);
		return (NULL);
	}
	return (path);
}
