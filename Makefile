# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cumberto <cumberto@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/07/08 13:11:13 by cumberto          #+#    #+#              #
#    Updated: 2018/04/30 01:49:24 by tchapka          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

SRC_NAME =	main.c \
			error_m.c \
			blank.c \
			cd.c \
			echo.c \
			env.c \
			setenv.c \
			env_attr.c \
			exec.c \
			free.c \
			path_fix.c \
			history.c \
			delete.c \
			copypaste.c \
			select_move.c \
			lexer5.c \
			parser.c \
			ast.c \
			exec3.c \
			pipe.c \
			redirection.c \
			signal.c \
			lexer_chr_read.c \
			singleton.c \
			heredoc.c \
			arrow.c \
			quotes.c \
			homend.c \
			cursor_mov.c \
			util.c \
			copypaste_util.c \
			history2.c \
			other.c \
			wait.c 

SRC_PATH = srcs

SRC = $(addprefix $(SRC_PATH)/,$(SRC_NAME))

OBJ_NAME = $(SRC_NAME:.c=.o)

OBJ_PATH = obj

OBJ = $(addprefix $(OBJ_PATH)/,$(OBJ_NAME))

INCLUDES = -Iincludes

NAME = 21sh

CFLAGS = -Wall -Werror -Wextra

all: $(NAME)

$(NAME): libft/libft.a $(OBJ)
	gcc $(INCLUDES) $(CFLAGS) $(OBJ) libft/libft.a -lncurses -o $(NAME)

libft/libft.a:
	make -C ./libft

$(OBJ_PATH)/%.o:$(SRC_PATH)/%.c
	mkdir -p $(OBJ_PATH)
	gcc $(CFLAGS) $(INCLUDES) -c $< -o $@

clean:
	make -C ./libft clean
	rm -Rf $(OBJ_PATH)

fclean: clean
	make -C ./libft fclean
	rm -f minishell

re: fclean libft/libft.a all

test: libft/libft.a
	gcc -g $(CFLAGS) $(SRC) $(addprefix $(BUILT_IN_PATH)/,$(BUILT_IN_SRC)) $(INCLUDES) libft/libft.a -lncurses -o $(NAME)
