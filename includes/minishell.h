/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <cumberto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 13:20:56 by cumberto          #+#    #+#             */
/*   Updated: 2018/04/30 01:36:15 by tchapka          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# include <unistd.h>
# include <stdlib.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <sys/wait.h>
# include <sys/ioctl.h>
# include <fcntl.h>
# include <dirent.h>
# include <term.h>
# include <curses.h>
# include "../libft/includes/libft.h"
# include <stdbool.h>
# include <signal.h>
# define SIZE 2048
# define EXE_NAME "21sh"
# define PATH_SIZE 1024
# define HSTR ".21sh_history"
# define CURSOR_UP(x) ft_printf("\033[%dA", (x))
# define CURSOR_DOWN(x) ft_printf("\033[%dB", (x))
# define CURSOR_FORE(x) ft_printf("\033[%dC", (x))
# define CURSOR_BACK(x) ft_printf("\033[%dD", (x))
# define CURSOR_SAVE "\033[s"
# define CURSOR_REST "\033[u"
# define K_UP 4283163
# define K_DOWN 4348699
# define K_RIGHT 4414235
# define K_LEFT 4479771
# define K_HOME 4741915
# define K_END 4610843
# define SHIFT_LEFT		17458
# define SHIFT_RIGHT	17202
# define RED   "\x1B[31m"
# define GRN   "\x1B[32m"
# define YEL   "\x1B[33m"
# define BLU   "\x1B[34m"
# define MAG   "\x1B[35m"
# define CYN   "\x1B[36m"
# define WHT   "\x1B[37m"
# define RESET "\x1B[0m"

typedef enum		e_astp
{
	AST_SEMIC,
	AST_DAMP,
	AST_AMP,
	AST_DPIPE,
	AST_PIPE,
	AST_CMD,
	AST_NULL
}					t_astp;

typedef enum		e_tokenlist
{
	T_SEMIC,
	T_DAMP,
	T_AMP,
	T_DPIPE,
	T_PIPE,
	T_REDI,
	T_REDI_AP,
	T_DUPI,
	T_REDO,
	T_REDO_AP,
	T_DUPO,
	T_BACKSL,
	T_NUM,
	T_WORD,
	T_CMD,
	T_ERRTKN,
	T_NEW,
	T_NULL
}					t_tokenlist;

typedef enum		e_mode
{
	M_GEN,
	M_QUOT,
	M_DQUOT,
}					t_mod;

typedef enum		e_lexem
{
	C_GEN,
	C_NUM,
	C_TOK,
	C_QUOTE,
	C_DQUOTE,
	C_SPACE,
	C_ESCAPE,
	C_NULL
}					t_lexem;

typedef struct		s_edit
{
	unsigned int	col;
	unsigned int	row;
	int				prompt_len;
	char			*cmd;
	unsigned int	cursor;
	unsigned int	max_size;
	char			*clp;
	int				clp_size;
	struct s_hstr	*h_last;
	struct s_hstr	*h_curr;
	struct s_hstr	*h_first;
}					t_edit;

typedef struct		s_hstr
{
	char			*cmd;
	struct s_hstr	*up;
	struct s_hstr	*down;
}					t_hstr;

typedef struct		s_dfd
{
	unsigned int	in;
	unsigned int	out;
	unsigned int	err;
}					t_dfd;

typedef struct		s_token
{
	char			*data;
	struct s_token	*next;
	t_tokenlist		type;
}					t_token;

typedef struct		s_lexer
{
	t_token			*first;
	t_token			*cur;
	t_mod			mod;
	int				size;
	int				cnt;
}					t_lexer;

typedef struct		s_ast
{
	t_token			*list;
	t_astp			type;
	unsigned int	tkn_num;
	char			**cmd;
	struct s_ast	*left;
	struct s_ast	*right;
}					t_ast;

int					ft_env(char **arg, t_ast *ast);
int					ft_execute(t_ast *node, unsigned int *cnt);
int					ft_cd(char **arg);
int					ft_setenv(char **arg);
char				*set_env(char *var, char *name);
int					ft_unsetenv(char **args);
int					error_msg_m(int err, char *arg, char *proc_name);
void				set_key_env();
void				unset_key_env();
int					chr_interpreter(char chr);
int					read_cmd_line(t_edit *line, int opt,
	unsigned int prompt_len);
char				*find_pattern(char *pattern);
char				*fix_path(char *path);
char				**new_env();
void				free_table(char **table);
int					ft_echo(char **args);
char				*path_manager(char *cmd);
char				*fix_path(char *path);
int					env_name_cmp(char *env, char *name);
void				ft_exit(char **args);
int					ft_add_history(t_edit *line);
void				ft_init_hstr(t_edit *line);
int					ft_print_history(t_edit *line);
void				ft_arrow_down(t_edit *line);
void				ft_arrow_up(t_edit *line);
void				ft_free_history(t_edit *line);
int					ft_open_hstr(void);
int					new_hstr(t_hstr **nw, char *str);
int					deletechar(t_edit *line);
int					lexer_analyzer(char *input, t_token **token, int size);
void				print_token(t_token *token);
t_ast				*parse_token(t_token *token, t_ast *ast);
t_ast				*make_ast(t_ast *token, t_ast *delim, t_tokenlist prio);
t_ast				*newnode(t_ast *list);
t_astp				find_my_type(t_tokenlist type);
void				free_ast(t_ast *ast);
char				*check_exec(char *cmd);
int					check_if_builtin(char *cmd);
void				execute(t_ast *node, unsigned int count);
void				execs(t_ast *node);
void				ft_execute_ast(t_ast *ast);
int					make_redirection(t_token *token, unsigned int size);
int					ft_exec_pipe(t_ast *node, int input, unsigned int *cnt);
int					ft_wait_me();
int					ft_pipe_cmd(t_ast *ast, unsigned int *count);
int					ft_eval_cmd(t_ast *ast, unsigned int *count);
char				*ft_signal_str(int sig);
int					error_bad(int err, char *arg, char *proc_name);
void				ft_exit_code(int *res, char *name);
t_token				*new_token(t_lexer *lexer);
t_edit				*get_line(void);
t_dfd				*get_fd(void);
int					fatal_error(int err);
void				free_token(t_token *token);
char				*reset_cmd(char *cmd, int size);
void				make_heredoc(t_ast *node);
int					escape_chr(t_edit *line, int ch);
int					ft_wait_direct_cmd(pid_t pid, char **av);
int					get_the_prompt();
t_edit				*reset_line(t_edit *line, unsigned int prompt_len);
int					check_quote(t_ast *node);
int					rec_key(t_edit *line, int ch);
t_edit				*init_cmd_line(t_edit *line);
int					is_up_k(t_edit *line);
int					is_do_k(t_edit *line);
int					is_ri_k(t_edit *line);
int					is_le_k(t_edit *line);
int					end_key(t_edit *line);
int					home_key(t_edit *line);
int					left_word(t_edit *line);
int					right_word(t_edit *line);
int					eof_handler(t_edit *line, int opt);
int					printchar(t_edit *line, char ch);
int					ft_strchrn(char *str, char ch);
char				*copy_until(char *dest, char *src, char ch);
int					reset_clp(t_edit *line);
int					ctrl_c_m(t_edit *line, int cnt);
void				cursor_old_pos(t_edit *line, int size);
void				reset_line_cmd(t_edit *line);
int					exit_clp(t_edit *line, int cnt, char buf);
char				**cp_env(void);
int					print_env(char **env);
int					parent_handler(int sig);
void				handler_pid(int sig);
void				p_handler(int sig);
void				win_handler(int sig);
int					chr_is_token(t_lexer *lexer, char chr);
void				init_fd(void);
void				reset_fd(void);
void				clean_exit(int ret);
t_lexer				init_lexer(t_lexer *lexer, int size);
bool				exec_builtin(char **cmd, t_ast *ast);
int					putsomechar(int ch);
int					ft_select_mode(t_edit *line);
int					ft_paste(t_edit *line);
int					ft_cut(t_edit *line);
int					ft_select_right(t_edit *line, int cnt);
int					ft_select_left(t_edit *line, int cnt);
int					ft_select_up(t_edit *line, int cnt);
int					ft_select_down(t_edit *line, int cnt);
void				pipe_handler(int sig);

pid_t g_pid;
int		g_ret;
char **g_env;
bool				g_slctm;

#endif
